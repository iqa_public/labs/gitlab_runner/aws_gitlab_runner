terraform {
  backend "s3" {
    encrypt        = true
    bucket         = "gitlab-runner-terraform-state-file-storage"
    dynamodb_table = "gitlab-runner-terraform-state-lock"
    key            = "terraform.tfstate"
  }
}
