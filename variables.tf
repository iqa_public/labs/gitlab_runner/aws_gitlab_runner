variable "aws_profile" {
  description = "AWS profile."
  type        = string
  default     = "default"
}

variable "aws_region" {
  description = "AWS region."
  type        = string
  default     = "ap-southeast-2"
}

variable "cache_bucket_prefix" {
  description = "Prefix for s3 cache bucket name."
  type        = string
  default     = "gitlab-runner-"
}

variable "ci_job_id" {
  description = "GitLab's job id"
  type        = string
  default     = ""
}

variable "description" {
  description = "A description for the runner."
  type        = string
  default     = "Shared GitLab Runner - auto"
}

variable "environment" {
  description = "A name that indentifies the environment, will be used as prefix and for tagging."
  type        = string
  default     = "ci-runners"
}

variable "gitlab_url" {
  description = "URL of the gitlab instance to connect to."
  type        = string
  default     = "https://gitlab.com/"
}

variable "locked_to_project" {
  description = "Runner is locked to a project"
  type        = string
  default     = "false"
}

variable "maximum_timeout" {
  description = "Maximum time before runner is considered offline"
  type        = string
  default     = "1800"
}

variable "private_ssh_key_filename" {
  default = "generated/id_rsa"
}

variable "public_ssh_key_filename" {
  default = "generated/id_rsa.pub"
}

variable "registration_token" {
}

variable "run_untagged" {
  description = "Allows runner to execute any job"
  type        = string
  default     = "true"
}

variable "runners_image" {
  description = "Image to run builds, will be used in the runner config.toml"
  type        = string
  default     = "docker:19.03.5"
}

variable "runners_name" {
  description = "Name of the runner, will be used in the runner config.toml"
  type        = string
}

variable "tag_list" {
  description = "By tagging a Runner, you can make sure shared Runners will only run the jobs they are equipped to run."
  type        = string
  default     = ""
}

variable "timezone" {
  description = "Name of the timezone that the runner will be used in."
  type        = string
  default     = "UTC"
}
