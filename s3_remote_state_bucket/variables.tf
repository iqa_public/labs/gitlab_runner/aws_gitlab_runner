variable "aws_profile" {
  description = "AWS profile."
  type        = string
  default     = "default"
}

variable "aws_region" {
  description = "AWS region."
  type        = string
  default     = "ap-southeast-2"
}

variable "remote_state_bucket_prefix" {
  description = "Prefix for s3 cache bucket name."
  type        = string
  default     = "gitlab-runner"
}
