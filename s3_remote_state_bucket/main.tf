data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_s3_bucket" "terraform_state_s3_bucket" {
  bucket = "${var.remote_state_bucket_prefix}-terraform-state-file-storage"

  versioning {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
  }

  tags = {
    Name = "Terraform State File Storage"
  }
}

# create a dynamodb table for locking the state file
resource "aws_dynamodb_table" "dynamodb-terraform-state-lock" {
  name = "${var.remote_state_bucket_prefix}-terraform-state-lock"
  hash_key = "LockID"
  read_capacity = 20
  write_capacity = 20

  attribute {
    name = "LockID"
    type = "S"
  }
}
