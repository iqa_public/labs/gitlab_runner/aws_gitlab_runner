data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_eip" "nat" {
  vpc   = true
  count = 1
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.21.0"

  name = "vpc-${var.ci_job_id}${var.environment}"
  cidr = "192.168.0.0/16"

  azs             = [data.aws_availability_zones.available.names[0]]
  public_subnets  = ["192.168.0.0/24"]
  private_subnets = ["192.168.1.0/24"]

  enable_nat_gateway = true
  single_nat_gateway = true
  enable_s3_endpoint = true

  tags = {
    Environment = "${var.ci_job_id}${var.environment}"
  }
}

module "runner" {
  source  = "npalm/gitlab-runner/aws"
  version = "4.10.0"

  aws_region  = var.aws_region
  environment = "${var.ci_job_id}${var.environment}"

  vpc_id                   = module.vpc.vpc_id
  subnet_ids_gitlab_runner = module.vpc.private_subnets
  subnet_id_runners        = element(module.vpc.private_subnets, 0)

  runners_name             = var.runners_name
  runners_gitlab_url       = var.gitlab_url
  enable_runner_ssm_access = true

  docker_machine_spot_price_bid = "0.06"

  gitlab_runner_registration_config = {
    registration_token = var.registration_token
    tag_list           = var.tag_list
    description        = "${var.ci_job_id}_${var.environment}_${var.description}"
    locked_to_project  = var.locked_to_project
    run_untagged       = var.run_untagged
    maximum_timeout    = var.maximum_timeout
  }

  tags = {
    "tf-aws-gitlab-runner:example"           = "runner-default"
    "tf-aws-gitlab-runner:instancelifecycle" = "spot:yes"
  }

  runners_off_peak_timezone   = var.timezone
  runners_off_peak_idle_count = 0
  runners_off_peak_idle_time  = 60

  runners_privileged         = "true"
  runners_additional_volumes = ["/certs/client"]

  runners_volumes_tmpfs = [
    { "/var/opt/cache" = "rw,noexec" },
  ]

  runners_services_volumes_tmpfs = [
    { "/var/lib/mysql" = "rw,noexec" },
  ]
  # working 9 to 5 :)
  runners_off_peak_periods = "[\"* * 0-9,17-23 * * mon-fri *\", \"* * * * * sat,sun *\"]"
}

// 2019.12.04 - tre: TODO This needs further investigation how we can leverage the script if we are not using
// the code base for this module.
//resource "null_resource" "cancel_spot_requests" {
//  # Cancel active and open spot requests, terminate instances
//
//  provisioner "local-exec" {
//    when    = destroy
//    command = "../../ci/bin/cancel-spot-instances.sh ${var.environment}"
//  }
//}
