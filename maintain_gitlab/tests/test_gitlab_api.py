from gitlab_api import ci_job_id, parse_arguments


def test_ci_job_id_wrong_file_name():
    error_string = ""

    try:
        ci_job_id("wrong_file_name")
    except FileNotFoundError as _file_not_found_error:
        error_string = str(_file_not_found_error)

    assert error_string == "Could not find file:\twrong_file_name"


def test_parse_arguments_no_arguments():
    _expected_error = "Error: the following arguments are required: -e/--environment, -s/--server, -t/--token\n"
    _error_string = None

    try:
        _expected_error = parse_arguments()
    except ValueError as _value_error_exception:
        _error_string = str(_value_error_exception)

    assert _expected_error == _error_string
