import iqa_argument_parser


def test_error():
    _parser = iqa_argument_parser.IqaArgumentParser()
    _error_string = None
    _expected_error_string = (
        "Error: the following arguments are required: -te/--test_error\n"
    )
    _parser.add_argument(
        "-te", "--test_error", required=True, help="Test Error Message"
    )

    try:
        vars(_parser.parse_args())
    except ValueError as _value_error_exception:
        _error_string = str(_value_error_exception)

    assert _expected_error_string == _error_string
