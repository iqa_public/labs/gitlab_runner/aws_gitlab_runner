#!/usr/bin/env python3
"""Sub-class of ArgumentParser"""
import sys as _sys
from argparse import ArgumentParser
from gettext import gettext as _


class IqaArgumentParser(ArgumentParser):
    """Changing behaviour in error case. Do not exit, rais an error."""

    def error(self, message):
        """error(message: string)

        Prints a usage message incorporating the message to stderr and
        exits.

        If you override this in a subclass, it should not return -- it
        should either exit or raise an exception.
        """
        self.print_usage(_sys.stderr)
        args = {"prog": self.prog, "message": message}
        raise ValueError(_("Error: %(message)s\n") % args)
