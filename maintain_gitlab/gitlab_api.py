#!/usr/bin/env python3
"""This script removes the registered runner instances in GitLab after they
have been destroyed in AWS"""
import os
import sys

import gitlab
from iqa_argument_parser import IqaArgumentParser


def main():
    """
    python3 -e <environment> -s <server_url> -t <gitlab_token>
    """

    args = parse_arguments()
    file_name_ci_job_id = "../.ci_job_id_plan"
    gitlab_instance = gitlab.Gitlab(args.get("server"), args.get("token"))

    runners_to_delete = []

    environment = args.get("environment")
    _ci_job_id = ci_job_id(file_name_ci_job_id)
    runner_description = _ci_job_id.strip() + "_" + environment
    print("Environment:\t\t", environment)
    print("Runner description:\t", runner_description)

    for runner in runners(gitlab_instance):
        description = runner.attributes.get("description")
        if runner_description in str(description):
            runners_to_delete.append(runner)
            print("Found runner:\t\t", description, " to delete")

    for runner in runners_to_delete:
        description = runner.attributes.get("description")
        print("Runner delete:\t " + description)
        runner.delete()

    for runner in runners(gitlab_instance):
        description = runner.attributes.get("description")
        print("Found runner:\t\t", description)


def ci_job_id(file_name_ci_job_id):
    """Read CI job id from file"""
    if os.path.exists(file_name_ci_job_id):
        with open(file_name_ci_job_id) as file_ci_job_id:
            ci_job_id_lines = file_ci_job_id.readlines()
    else:
        raise FileNotFoundError("Could not find file:\t" + file_name_ci_job_id)
    if len(ci_job_id_lines) == 0:
        sys.exit(1)
    print("CI_JOB_ID:\t\t", ci_job_id_lines[0])
    return ci_job_id_lines[0]


def runners(gitlab_instance):
    """Returns a list of GitLab runners"""
    return gitlab_instance.runners.list()


def parse_arguments():
    """Validate arguments have been provided as expected."""
    parser = IqaArgumentParser()
    parser.add_argument(
        "-e",
        "--environment",
        required=True,
        help="Environment name used to prefix labels and resources",
    )
    parser.add_argument(
        "-s",
        "--server",
        required=True,
        help="GitLab server address (e.g. https://gitlab.com)",
    )
    parser.add_argument("-t", "--token", required=True, help="GitLab auth token")
    args = vars(parser.parse_args())
    return args


if __name__ == "__main__":
    sys.exit(main())
