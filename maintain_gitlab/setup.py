"""This is the default script to configure Python modules"""
#!/usr/bin/env python

import os.path

from setuptools import find_packages, setup

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
# VERSION_FILE = os.path.join(BASE_DIR, 'phish_phillets', 'version.py')


# def get_version():
#     with open(VERSION_FILE) as f:
#         for line in f.readlines():
#             if line.startswith("__version__"):
#                 version = line.split()[-1].strip('"')
#                 return version
#         raise AttributeError("Package does not have a __version__")


setup(
    name="maintain_gitlab",
    # version=get_version(),
    description="This application is used to ",
    author="Thomas Recker",
    author_email="thomas.recker@integrationqa.com",
    maintainer="Operations Team",
    maintainer_email="tech-user@integrationqa.com",
    keywords="gitlab maintain api",
    packages=find_packages(exclude=["*.test", "*.test.data"]),
    install_requires=[
        "virtualenv==16.7.9",
        "python-gitlab==2.0.1",
        "pylint==2.4.4",
        "pytest==5.3.5",
        "pytest-cov==2.8.1",
    ],
    # entry_points={
    #     'console_scripts': [
    #         'phish_phillets = phish_phillets.__init__:run',
    #         'pp_imap_fetch = phish_phillets.pp_imap_fetch.__init__:run',
    #         'pp_api_sync = phish_phillets.pp_api_sync.__init__:run',
    #         'pp_cif_publisher = phish_phillets.pp_cif_publisher.app:run',
    #         'pp_apwg_publisher = phish_phillets.pp_apwg_publisher.app:run',
    #     ],
    # },
    extras_require={"test": ["coverage", "pytest", "pytest-cov", "tox"]},
)
