provider "aws" {
  profile = var.aws_profile
  region  = var.aws_region
  version = "~> 2.8"
}

provider "template" {
  version = "~> 2.1.2"
}

provider "local" {
  version = "~> 1.4.0"
}

provider "null" {
  version = "~> 2.1.2"
}

provider "tls" {
  version = "~> 2.1.1"
}

