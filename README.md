# AWS - GitLab Runner

This project will create a stack in AWS which allows to autoscale GitLab
build runners based on spot instances.

The implementation is based on the Terraform module ["npalm/gitlab-runner/aws"](https://github.com/npalm/terraform-aws-gitlab-runner)
where you can find further information in case you are interested.

The following diagram shows the high level design as taken from the original repository.

![GitLab Runners](https://github.com/npalm/assets/raw/master/images/terraform-aws-gitlab-runner/runner-default.png)

[Source image](https://github.com/npalm/assets/raw/master/images/2017-12-06_gitlab-multi-runner-aws.png)

For more information [continue reading on the wiki](https://gitlab.com/iqa_public/labs/gitlab_runner/aws_gitlab_runner/wikis/home).
